.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Les nouveautés de ToPIA 3.0
===========================


Modularisation de la génération des TopiaId
-------------------------------------------

La forme des topiaIds générés à changé. Si vous souhaitez conserver l'ancienne forme parce que vous
souhaitez conserver l'uniformité avec une base de données existante ou si vous utilisez
topia-service-security, vous devez explicitement le spécifier à Topia 3.0 via la configuration :

    ``topia.persistence.topiaIdFactoryClassName=org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory``

Le classe **TopiaID** est dépréciée, elle sera supprimée dans les prochaines versions.

::

  FIXME tchemit : je ne suis pas convaincu par ça, le module security pour moi doit aussi être déprécié, donc la dernière phrase est caduc.


TopiaEntities enrichi
---------------------

TODO
