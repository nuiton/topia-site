.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=====
ToPIA
=====

Buts
====

- Abstraction de la persistence
- Sauvegarde/restauration en XML
- Sécurité sur les instances d'objets
- Génération de code même si ce n'est pas un prérequis pour utiliser ToPIA

Ce qu'il serait bien de récuperer par rapport à la version 2

- support des sous-transactions

Peut-être plus tard

- Gestion des services
- Distribution transparente

La vision ToPIA
===============

Un point d'entrée le TopiaContext sur lequel on ouvre des transactions ce qui
retourne un autre TopiaContext à partir duquel on récupère les DAO des
différentes entités qui permettent de faire les traitements que l'on souhaite.

Ensuite on peut appeler la methode commit de ce sous TopiaContext pour mettre
à jour la base de données et permettre à d'autres TopiaContext d'avoir la 
nouvelle vision de nos objets.

Chaque TopiaContext créé sur le TopiaContext root est indépendant.

Lorsque l'on travaille avec un objet provenant d'un TopiaContext sur lequel
on a fait un rollback, celui-ci n'est plus valide et il vaut mieux en recupérer
une version correcte sur le TopiaContext.

Après avoir fait un commit ou un rollback sur un TopiaContext on peut continuer
a l'utiliser et refaire des commits pour synchroniser de temps en temps les 
modification faites sur la base.

Un TopiaContext peu servir aussi longtemps que l'on souhaite, il ne maintient
pas inutilement de transaction sur la base.
