.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

========
sécurité
========

Implantation de la sécurité
===========================

La sécurité est un simple objet java qui hérite de TopiaHelper

Il se met alors listener sur tous les events vetoables et lève des exceptions
si la personne qui essaie de charger/créer/modifier/supprimer l'objet n'en a pas
le droit.

Le service de sécurité vient avec son fichier de configuration. Dans ce fichier
il y a les paramètres utiles au service pour retrouver les informations sur les
droits.

TopiaHelper est en fait une interface qui contient la méthode
init(TopiaContext, Properties)


package
-------

:org.nuiton.topia.security: pour tout ce qui touche a la sécurité
  (TopiaAuthenticationHelper, TopiaAuthorisationHelper, LoginModule, Filter, ...)
:org.nuiton.topia.security.entities: pour les entities utiles à la définition
  de la sécurité (user, group, permission)
