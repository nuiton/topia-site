.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Détails de l'intégration avec Maven
===================================

TODO


Comme il s'agit d'une phase de compilation, Maven va ajouter tout le code généré aux
classpath le code généré sera compilé et lié en même temps que le reste de votre application.
Tous les fichiers .class se retrouveront dans target/classes, dans la suite du processus, on
ne distingue plus le code généré du code utilisateur.

Par la suite, si vous changez le modèle, tout sera re-généré. À chaque ``clean``
via Maven, tout ce qui a été généré est effacé. Vous pouvez donc re-généré autant
de fois que nécessaire. Attention, Maven ne détecte pas le changement du modèle
si le fichier .zargo a été modifié : il faut donc faire un clean à chaque fois
afin de forcer le re-génération du modèle dans sa dernière version.

