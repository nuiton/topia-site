.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as
.. * published by the Free Software Foundation, either version 3 of the
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -
Comment personnaliser la génération de code ?
=============================================

TODO


Les données associées aux éléments du modèle
--------------------------------------------

Le modeleur n'est parfois pas suffisant pour décrire toute les subtilités du modèle. Le modèle se voulant indépendant de
la plate-forme cible, on ne peut y inclure directement des notions spécifiques à ToPIA. C'est pourquoi nous allons
pouvoir ajouter ces informations spécifiques via le fichier ``src/main/resources/library-config.properties``.


