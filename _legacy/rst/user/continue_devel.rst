.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==========================
Continuer le développement
==========================

Vous avez maintenant les clés pour développer votre application. Voici quelques
éléments qui vous permettront de poursuivre les développements.

Gestion des évènements
======================

TODO

Les visiteurs d'entités
=======================

TODO

Écrire une migration
====================

TODO

Débugguer l'application
=======================

  * Ajouter ``-Deugene.verbose`` sur la ligne de commande pour voir ce que
    EUGene fait pendant la phase de génération.

  * Ne pas hésiter à examiner les fichiers ``.hbm.xml`` générés pour voir
    s'ils correspondent bien à ce que vous attendiez. De même, on
    peut examiner le fichier ``.objectmodel`` pour voir si l'interprétation
    du fichier zargo est bien celle qu'on attend.
