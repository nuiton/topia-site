.. -
.. * #%L
.. * ToPIA
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2014 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

==========================
Frequently Asked Questions
==========================

Problème lors du chargement d'une entity
========================================

Le chargement utilise les méthodes Set des propriétés, il faut donc faire
attention si on les surcharge.

Il faut faire attention lors de l'ajout, sur l'entity, de méthodes qui ont la
même signature que des méthods Set de propriétés avec juste des arguments
différents, car elles peuvent être utilisées à la place de la vrai méthode
pour quelque valeur (par exemple null).

Faire fonctionner les Web Services Topia via RMI
================================================
L'utilisation des Web Services sur RMI impose la génération de classes sauvées
sur disque.
Elle sont construites à la volée et sauvées dans le dossier "./topiagen".

Ce dossier doit donc se trouver dans le classpath.

Comment commit (ou rollback) une transaction directement à partir d'un DAO ?
============================================================================

Contrairement à ce que l'on pourrait croire aux premiers abords, les méthodes commitTransaction()
et rollbackTransaction() disponibles dans les DAO ne sont pas prévus à cet effet. Il faut toujours
utiliser le contexte pour commit (ou rollback) une transaction. Donc pour le faire à partir d'un DAO :
dao.getContext().commitTransaction() // on récupère le context (transaction sur le DAO) et on commit.
Le principe reste identique pour le rollback.
Pour revenir sur les méthodes commitTransaction() et rollbackTransaction() disponibles sur le DAO, elles
servent de "callback" au moment du commit (ou rollback) de la transaction. Le context se chargera d'appeler
tous les commitTransaction() des DAO, ceci afin de permettre un traitement spécifique au DAO au moment du commit.
Ce traitement peut donc être effectué directement à partir du dao via cette méthode sans passer par le context.

