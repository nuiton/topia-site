.. -
.. * #%L
.. * ToPIA :: Persistence
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2011 CodeLutin, Chatellier Eric
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

======
How to
======

Ce document a pour objectif de prendre en main le framework Topia. Le document
est constitué de 4 parties, la modélisation, la génération, la configuration et
l'utilisation.

Modélisation
------------

La première étape constiste à réaliser le modèle UML soit avec Argouml 
(http://argouml.tigris.org) soit avec Poséidon (http://www.gentleware.com).

Le stérotype «entity» sur les classes permet de préciser que la classe est
persistée par Topia. D'autre stérotypes (extern, service, ...) sont disponibles
mais ne sont pas présentés.

Le fichier de modélisation peut être complété par un fichier de configuration
nommé <nom fichier modélisation>.properties. Il contient des informations
techniques propres aux librairies utilisé par Topia.

Exemple :

::

  # Précise comment est mappé un type
  # model.tagvalue.<type java>=<type base de donnée>
  model.tagvalue.java.lang.String=text
  
  # Précise le schema de la base de données par défaut
  # model.tagvalue.dbSchema=<nom schéma>
  model.tagvalue.dbSchema=test
  
  # Précise l'entête de l'ensemble des fichiers générés
  # model.tagvalue.copyright=<valeur>
  model.tagvalue.copyright=/* *##%\n Copyright (C) 2011 Code Lutin\n *##% */
  
  # Force le chargement d'un attribut
  # <nom paquetage>.<nom entité>.attribute.<nom attribut>.tagvalue.lazy=false
  org.codelutin.Person.attribute.association.tagvalue.lazy=false

Génération
----------

La génération des fichiers Topia se font par l'utilisation du plugin maven
Eugene_. Ce plugin est disponible sur le repository central.

Configuration du plugin maven :

::

  <plugin>
    <groupId>org.nuiton.eugene</groupId>
    <artifactId>eugene-maven-plugin</artifactId>
    <version>${eugeneVersion}</version>
    <executions>
        <execution>
            <phase>process-sources</phase>
            <!--Configuration of model generator-->
            <configuration>
                <inputs>zargo</inputs>
                <fullPackagePath>org.nuiton.topiatest</fullPackagePath>
                <defaultPackage>org.nuiton.topiatest</defaultPackage>
                <extractedPackages>org.nuiton.topiatest</extractedPackages>
                <templates>
                    org.nuiton.topia.templates.TopiaMetaTransformer
                </templates>
            </configuration>
            <goals>
                <goal>generate</goal>
            </goals>
        </execution>
    </executions>
    <dependencies>
        <dependency>
            <groupId>org.nuiton.topia</groupId>
            <artifactId>topia-persistence</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
  </plugin>

Le plugin utilise Eugene_ avec les templates de génération de Topia, le
fichier argouml ou poséidon doit se trouver dans le répertoire
``src/main/xmi``. Les fichiers sont générés dans le répertoire
``target/generated-sources/java``. Il faut préciser au plugin les paquetages
à traiter avec le ``defaultPackage`` et ``extractedPackages``.

Ajout dans des dépendances de Topia pour la compilation des fichiers générés :

::

  <dependency>
    <groupId>org.nuiton.topia</groupId>
    <artifactId>topia-persistence</artifactId>
    <version>${project.version}</version>
    <scope>compile</scope>
  </dependency>

Pour une classe modélisée, nous retrouvons 6 fichiers :
  * Une interface
  * Une classe abstraite
  * Une implantation
  * Une configuration hibernate
  * Un DAO abstrait
  * Une implantation du DAO

Une classe Helper est aussi générée, permettant l'accès à l'ensemble des DAO.

Configuration
-------------

Un fichier contenant les informations de connexion à la base de données et de
configuration de Topia doit être créé au niveau des ressources du projet.
Par convention, ce fichier est nommé ``TopiaContextImpl.properties``.


Utilisation
-----------

Implantation des méthodes
~~~~~~~~~~~~~~~~~~~~~~~~~

Si des méthodes ont été définies dans le modèle de données, il faut créer
l'implantation correspondante. Le paquetage doit porter le même nom que celui de
la génération.



.. _Eugene: http://doc.nuiton.org/eugene/
