package com.company;

/*
 * #%L
 * ToPIA
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2004 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaConfigurationConstants;

import com.company.app.MyLibraryTopiaApplicationContext;
import com.company.app.MyLibraryTopiaPersistenceContext;
import com.company.app.entities.Author;
import com.company.app.entities.AuthorTopiaDao;
import com.company.app.entities.Book;
import com.company.app.entities.BookTopiaDao;

public class AppTest {

    @Test
    public void testApp() {
        // Declaration de la configuration
        Map<String, String> config = new HashMap<String, String>();
        config.put(TopiaConfigurationConstants.CONFIG_DRIVER, "org.h2.Driver");
        config.put(TopiaConfigurationConstants.CONFIG_DIALECT, "org.hibernate.dialect.H2Dialect");
        config.put(TopiaConfigurationConstants.CONFIG_USER, "sa");
        config.put(TopiaConfigurationConstants.CONFIG_PASS, "");
        config.put(TopiaConfigurationConstants.CONFIG_URL, "jdbc:h2:file:/tmp/test-" + System.nanoTime());
        config.put(TopiaConfigurationConstants.CONFIG_PERSISTENCE_INIT_SCHEMA, "true");

        // Creation de l'ApplicationContext (va initialiser la base et les services de ToPIA)
        MyLibraryTopiaApplicationContext applicationContext =
                new MyLibraryTopiaApplicationContext(config);

        // Démarre une transaction représentée par le PersistenceContext
        MyLibraryTopiaPersistenceContext persistenceContext =
                applicationContext.newPersistenceContext();

        // On demande au PersistenceContext les instances des Dao
        AuthorTopiaDao authorDao = persistenceContext.getAuthorDao();
        BookTopiaDao bookDao = persistenceContext.getBookDao();

        Assert.assertEquals(0, authorDao.count());
        Assert.assertEquals(0, bookDao.count());

        { // On créé un author et on demande au Dao de le persister
            Author author = authorDao.newInstance();
            author.setName("Antoine de Saint-Exupéry");
            authorDao.create(author);
        }

        Assert.assertEquals(1, authorDao.count());

        { // On créé un livre en listant les propriétés qui le composent
            Author author = authorDao
                    .forNameEquals("Antoine de Saint-Exupéry")
                    .findUnique();
            bookDao.create(Book.PROPERTY_NAME, "Le petit prince",
                    Book.PROPERTY_AUTHOR, author);
        }

        Assert.assertEquals(1, bookDao.count());

        // On commite la transaction
        persistenceContext.commit();

        // Fermeture des contextes
        persistenceContext.close();
        applicationContext.close();
    }
}
