---
layout: page
title: "Qu'est-ce qu'un cycle de développement en Y ?"
category: docs
---


Le cycle de développement en Y traditionnel peut être représenté de la manière
suivante :

La branche fonctionnelle (porteuse de la solution logique) et la branche
technique (qui définit la plateforme technique sur laquelle la solution sera
implantée) se rejoignent en amont de la conception détaillée et du
codage. Cette projection peut être accompagnée de génération de code, mais il
reste toujours une certaine quantité de code écrite à la main.

Cette séparation des responsabilités et ce processsus donnent pleine satisfaction
lors du développement initial d'un logiciel. La vie d'un logiciel commence
toutefois au moment de sa mise en production et les évolutions qui y seront
apportées sont de deux natures :

  - Évolutions fonctionnelles : Ajout de fonctionnalités, correction de bugs, ...
  - Évolutions techniques : Changement de bases de données, changement de
    technologie sur les UI, nouveaux accès (SOAP ?), ...

Si les évolutions fonctionnelles sont prises en compte par la nature itérative
des processus de développement, les évolutions techniques sont elles difficiles.

Comment passer d'Hibernate à JDO, de Swing au client léger, des EJB aux
conteneurs légers ? En supprimant les dépendances de votre code sur les briques
techniques, en codant sur une plateforme technique abstraite, interfaces et
façades au travers desquelles vous pourrez manipuler les différentes solutions
techniques que vous retiendrez. Hibernate et JDO ne sont que deux API d'accès
aux données; Swing et client léger, des UI; les EJB et les conteneurs légers,
des logiques métiers déportées...
