---
layout: page
title: "Génération des modèles"
category: docs
---


Le module `topia-persistence` de ToPIA est capable de générer la persistence à partir d'un modèle UML.

Nous allons détailler ici comme inclure la génération dans le cycle de compilation maven.


Architecture maven
------------------

 * `src/main/xmi/mymodel-model.zargo` (exemple de modele UML avec argoUML)
 * `src/main/xmi/mymodel-model.properties` (fichier de propriétés attaché au modele)

Configuration du pom.xml
------------------------

Pour générer les sources dans la phase `generate-sources` de maven, et utiliser
les sources générées, et faut configurer le pom.

### Plugin Eugene


{% highlight xml %}
  <plugin>
    <groupId>org.nuiton.eugene</groupId>
    <artifactId>eugene-maven-plugin</artifactId>
    <version>{{site.eugeneVersion}}</version>
    <executions>
      <execution>
        <id>Generator</id>
        <phase>generate-sources</phase>
        <configuration>
          <inputs>zargo</inputs>
          <fullPackagePath>org.company.package</fullPackagePath>
          <extractedPackages>org.company.package</extractedPackages>
          <templates>org.nuiton.topia.generator.TopiaMetaGenerator</templates>
          <defaultPackage>org.company.package</defaultPackage>
        </configuration>
        <goals>
          <goal>generate</goal>
        </goals>
      </execution>
    </executions>
    <dependencies>
      <dependency>
        <groupId>org.nuiton.topia</groupId>
        <artifactId>topia-persistence</artifactId>
        <version>{{site.topiaVersion}}</version>
      </dependency>
    </dependencies>
  </plugin>
{% endhighlight %}

Pour plus d'information à propos d'Eugene, merci de consulter le site :
http://doc.nuiton.org/eugene/

### Dépendances du projet


Il faut enfin ajouter "topia-persistence" en dépendance du projet :


{% highlight xml %}
  <dependency>
    <groupId>org.nuiton.topia</groupId>
    <artifactId>topia-persistence</artifactId>
    <version>{{site.topiaVersion}}</version>
  </dependency>
{% endhighlight %}

