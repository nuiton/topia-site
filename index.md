---
layout: homepage
title:  "About ToPIA"
---

# What is ToPIA?

ToPIA stands "Tools for Portable and Independent Architecture", it's our try to
provide developers with a persistence framework which is

   - Quick-and-easy: minimal bootstrap code in your application, 80% of the database
   requests can be written in one line of pure Java. Your favorite IDE
   code-completion will give you intuitive help
   - Safe: most of the code is type-safe and our API enforce strong checks, no
   hard-coded SQL/HQL strings that will break only at runtime
   - Portable: we rely on Hibernate to be SGBD-portable, we do not require any
   framework (no spring, no JEE). ToPIA API is pure Java without any reference to
   persistence infrastructure. It has allowed some applications to migrate easily
   between JDO to Hibernate. Entities are plain Java Beans you can use everywhere
   in your application.

We rely on a MDD approach. The developer has to describe in persistence model in
a UML file and ToPIA do the rest.

# Why ToPIA?

We made ToPIA and we use it everyday in multiple projects because we were not satisfied with the existing solutions.

We built ToPIA with many strong rules and we think most of the available solutions does not follow them:

  - Static typing is one of the best functionality of Java, it should be embraced
  and used to help the developer to write sure code
  - Separation of concerns: business code (aka services) should be coded in plain Java
  without solving persistence issues.
  - Writing XML mappings, dozens of annotations, standards Java Beans, DAO
  classes with dummy CRUD operation is boring and time-consuming. Developers
  have better stuffs to do
  - We strongly believe in Free software and open-source code management. Thus,
  everything of ToPIA is made available under LGPL licence, we don't try to sell
  extras

# How does it work?

ToPIA relies on [EUGene](http://nuiton.page.nuiton.org/eugene/eugene/index.html) for code generation. ToPIA provides EUGene with templates
that to generate entities POJO, DAO implementations and few other things.

Most of the time, we use Eugene maven plugin, so the generated classes are
generated along the build and made available for compilation in the classpath
and readable just like plain code in your IDE.

To ensure extensibility, ToPIA itself and all the generated code is designed
to provide hooks (so override can be used to change any behavior). Also, we
use [Generation gap design pattern]: all the generated code is disposable and
separated of the hand-written code. You will never lost a piece of code by
regenerating. If Eugene finds that a file already exists in your source, it
assume that you wrote it yourself to fit your need and this class generation
will be skipped.

# Why should I use ToPIA?

You should not use ToPIA in the following case:

  - Your persistence infrastructure is not a relational SGBD (you want NoSQL)
  - Your model is dynamic: you don't know exactly what are the data the end-user
  will store and you don't want to be coerced to type everything everywhere
  - You're happy with Hibernate 4 or plain old JDBC: ToPIA was made when JDO
  was the primary persistence solution in the Java world and ToPIA 2 was based
  on Hibernate 3. ToPIA 3 is based on Hibernate 4 and it's clear that Java
  entities with annotations here and there works fine
  - The database already exists and the schema is already defined (by another
  application), you want your model to adapt to an existing schema, you can't
  change the schema. ToPIA is a model-centric framework, not a database-centric
  one. Maybe you should use a database-centric framework like [jOOQ].

# I'm convinced! How do I get started?

<div class="get-started-button">
<a href="{{site.baseurl}}/documentation/from_scratch.html" class="btn btn-primary">
  Discover how to get up and running with ToPIA
</a>
</div>

[Hibernate]: http://hibernate.org/
[Maven]:     http://maven.apache.org/
[Eugene]:    http://doc.nuiton.org/eugene/
[Flyway]:    http://flywaydb.org/
[Liquibase]: http://www.liquibase.org/
[Generation gap design pattern]: http://en.wikipedia.org/wiki/Generation_gap_(pattern)
[jOOQ]: http://www.jooq.org/

[Créer un projet ToPIA en partant de rien]:       {{site.baseurl}}/documentation/from_scratch.html
[migrate_to_topia_3]:                             {{site.baseurl}}/documentation/migrate_to_topia_3.html
[y_cycle]:                                        {{site.baseurl}}/docs/y_cycle.html
[Service de gestion des mises à jour (Flyway)]:   {{site.baseurl}}/documentation/topia_migration_flyway.html
