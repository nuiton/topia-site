---
layout: documentation
title: "Examples"
category: documentation
---

## The sample project

You can check out our sample project, we made it to make it easy for you to see how ToPIA is started and used. You can retrieve it using git.

{% highlight bash %}
git clone {{site.topiaSampleProjectGitRepository}}
{% endhighlight %}

## Real-life projects using ToPIA

ToPIA is ready for production. If you wan't to see how it is used in a developer every-day project, we have listed here projects we worked on.

### WAO

WAO is a web application based on HTML5, CSS3, jQuery, Twitter Bootstrap, Struts 2 and ToPIA. It's a modern-looking application which rely heavily on its database: the dataset is small but large computations results are expected in seconds.

You can browse WAO sources by checking out the [official WAO git repository].

[official WAO git repository]:   https://gitweb.codelutin.com/wao.git
