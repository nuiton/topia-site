---
layout: documentation
title: "ToPIA from scratch"
category: documentation
---

In this tutorial, we will see how ToPIA can be integrated in a minimal project, we want
to make you able to integrate ToPIA in you own project.

The easiest way to do that is to show you a working sample project, feel free to
copy some parts of it in your project to get things done.

It will be [Maven]-based since it's our favorite build tool but feel free to use yours.

## Get the sample project

You can download our sample project using git:

{% highlight bash %}
git clone {{site.topiaSampleProjectGitRepository}}
{% endhighlight %}

As you can see, only few files are sufficient

{% highlight bash %}
.
├── pom.xml
└── src
├── main
│   ├── java
│   │   └── com
│   │       └── company
│   │           └── MyLibraryApplication.java
│   └── xmi
│       └── library-model.zargo
└── test
    └── java
        └── com
            └── company
                └── SampleTest.java

10 directories, 5 files
{% endhighlight %}

It's an example project where we start implementing a project named "my-library".

## Open the model

In `src/main/xmi`, you will find a .zargo file. It's you persistence model and
you can open it using [ArgoUML].

![Library]({{site.baseurl}}/assets/library.png)

As you can see, the entities are modeled in a UML class diagram. We have named
them according to Java conventions and we added a stereotype `entity`, letting
Eugene know that this class is an entity and that everything must be generated
as such.

## Generate code from the model

Let's just call

{% highlight bash %}
mvn compile
{% endhighlight %}

As you can see, during the maven compile phase, EUGene is called:

<pre>
[INFO] --- eugene-maven-plugin:2.13:generate (generate-entities) @ my-library-application ---
[INFO] Process phase [zargo]
[INFO] Generate one file in 15,56ms.
[INFO] Process phase [xmi]
[INFO] Generate one file in 800,711ms.
[INFO] Process phase [model]
[INFO] Process phase [generator]
[INFO] Apply generator JavaInterfaceTransformer
[INFO] Apply generator JavaBeanTransformer
[INFO] Apply generator JavaEnumerationTransformer
[INFO] Apply generator TopiaMetaTransformer
[INFO] Generate 21 files in 170,423ms.
</pre>

Let's have a look at what was generated in the maven `target` directory:

<pre>
└── target
    ├── classes
    │   └── com
    │       └── company
    │           ├── app
    │           │   ├── AbstractMyLibraryTopiaApplicationContext.class
    │           │   ├── AbstractMyLibraryTopiaDao.class
    │           │   ├── AbstractMyLibraryTopiaPersistenceContext.class
    │           │   ├── entities
    │           │   │   ├── AbstractAuthorTopiaDao.class
    │           │   │   ├── AbstractBookTopiaDao.class
    │           │   │   ├── AuthorAbstract.class
    │           │   │   ├── Author.class
    │           │   │   ├── AuthorImpl.class
    │           │   │   ├── AuthorImpl.hbm.xml
    │           │   │   ├── AuthorTopiaDao.class
    │           │   │   ├── BookAbstract.class
    │           │   │   ├── Book.class
    │           │   │   ├── BookImpl.class
    │           │   │   ├── BookImpl.hbm.xml
    │           │   │   ├── BookTopiaDao.class
    │           │   │   ├── GeneratedAuthorTopiaDao.class
    │           │   │   └── GeneratedBookTopiaDao.class
    │           │   ├── MyLibraryEntityEnum.class
    │           │   ├── MyLibraryTopiaApplicationContext.class
    │           │   ├── MyLibraryTopiaDaoSupplier.class
    │           │   └── MyLibraryTopiaPersistenceContext.class
    │           └── MyLibraryApplication.class
    ├── generated-sources
    │   ├── java
    │   │   └── com
    │   │       └── company
    │   │           └── app
    │   │               ├── AbstractMyLibraryTopiaApplicationContext.java
    │   │               ├── AbstractMyLibraryTopiaDao.java
    │   │               ├── AbstractMyLibraryTopiaPersistenceContext.java
    │   │               ├── entities
    │   │               │   ├── AbstractAuthorTopiaDao.java
    │   │               │   ├── AbstractBookTopiaDao.java
    │   │               │   ├── AuthorAbstract.java
    │   │               │   ├── AuthorImpl.hbm.xml
    │   │               │   ├── AuthorImpl.java
    │   │               │   ├── Author.java
    │   │               │   ├── AuthorTopiaDao.java
    │   │               │   ├── BookAbstract.java
    │   │               │   ├── BookImpl.hbm.xml
    │   │               │   ├── BookImpl.java
    │   │               │   ├── Book.java
    │   │               │   ├── BookTopiaDao.java
    │   │               │   ├── GeneratedAuthorTopiaDao.java
    │   │               │   └── GeneratedBookTopiaDao.java
    │   │               ├── MyLibraryEntityEnum.java
    │   │               ├── MyLibraryTopiaApplicationContext.java
    │   │               ├── MyLibraryTopiaDaoSupplier.java
    │   │               └── MyLibraryTopiaPersistenceContext.java
</pre>

## Start ToPIA

The MyLibraryApplication as a main() method. You can try to call it, it should
works. You should read the body of the main method to understand how to start
ToPIA.

The big picture is: you need a DAO so, you need a MyLibraryTopiaPersistenceContext
so, you need a MyLibraryTopiaApplicationContext, so you need a [TopiaConfiguration].

Your very first goal is to create a TopiaConfiguration instance. The builder is
here to to help but you can just instantiate a BeanTopiaConfiguration. The
TopiaConfiguration instance will contain all information to connect to the
database.

Once your TopiaConfiguration object is created, you can instantiate
MyLibraryTopiaApplicationContext by calling the constructor and passing your
TopiaConfiguration (you no longer need it).

The [TopiaApplicationContext] is the abstract representation of a database you
want to work with. If your application works with two different database, you
need to instances of TopiaApplicationContext. On most project, you need to
create a TopiaApplicationContext when the application starts and close it when
the application stops.

Now that you have a TopiaApplicationContext, you can call newPersistenceContext():
it will give you a persistence context which represent an isolated access to the
database, you can think it as a transaction: you can commit it, rollback it, and
it can provide you with all the DAO available.

The DAO provided by the [TopiaPersistenceContext] will allow you to retrieve, create,
update and delete all the entities.

## Understand the code generation

There is no magic and Maven can generate all the required classes because EUGene
is declared in the project's pom.xml file.

The declaration is pretty long but we are working to reduce it and make it easier
to understand:

{% highlight xml %}
<properties>
  <!-- we strongly recommand to use a fixed version for all your dependencies -->
  <topiaVersion>[3.0-beta-14,)</topiaVersion>
  <eugeneVersion>2.13</eugeneVersion>
</properties>

<dependencies>
  <dependency>
    <groupId>org.nuiton.topia</groupId>
    <artifactId>topia-persistence</artifactId>
    <version>${topiaVersion}</version>
  </dependency>
  <dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <scope>test</scope>
    <version>4.11</version>
  </dependency>
</dependencies>

<build>
  <plugins>
    <plugin>
      <groupId>org.nuiton.eugene</groupId>
      <artifactId>eugene-maven-plugin</artifactId>
      <version>${eugeneVersion}</version>
      <configuration>
		<inputs>zargo</inputs>
		<resolver>org.nuiton.util.FasterCachedResourceResolver</resolver>
      </configuration>
      <executions>
        <execution>
          <id>generate-entities</id>
          <phase>generate-sources</phase>
          <configuration>
            <!-- Corresponding to extracted package from zargo file -->
            <fullPackagePath>com.company.app.entities</fullPackagePath>
            <!-- DefaultPackage used for Model classes generation (XxxEntityEnum, XxxTopiaXyz classes, ...)  -->
            <defaultPackage>com.company.app</defaultPackage>
            <templates>
              org.nuiton.eugene.java.JavaInterfaceTransformer,
              org.nuiton.eugene.java.JavaBeanTransformer,
              org.nuiton.eugene.java.JavaEnumerationTransformer,
              org.nuiton.topia.templates.TopiaMetaTransformer
            </templates>
          </configuration>
          <goals>
              <goal>generate</goal>
          </goals>
        </execution>
      </executions>
      <dependencies>
        <dependency>
          <groupId>org.nuiton.topia</groupId>
          <artifactId>topia-persistence</artifactId>
          <version>${topiaVersion}</version>
        </dependency>
        <dependency>
          <groupId>org.nuiton.topia</groupId>
          <artifactId>topia-templates</artifactId>
          <version>${topiaVersion}</version>
        </dependency>
      </dependencies>
    </plugin>
  </plugins>
</build>
{% endhighlight %}


[Maven]:   http://maven.apache.org/
[ArgoUML]: http://argouml.tigris.org/

[TopiaConfiguration]: {{site.apidocs}}?org/nuiton/topia/persistence/TopiaConfiguration.html
[TopiaApplicationContext]: {{site.apidocs}}?org/nuiton/topia/persistence/TopiaApplicationContext.html
[TopiaPersistenceContext]: {{site.apidocs}}?org/nuiton/topia/persistence/TopiaPersistenceContext.html
