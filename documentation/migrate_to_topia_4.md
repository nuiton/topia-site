---
layout: documentation
title: "Migrate from ToPIA 3.x to ToPIA 4.0"
category: documentation
---

This documentation section is french-only

## Gestion des dépendances

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Java 11 est désormais nécessaire.

ToPIA 4.0 nécessite des versions minimum de Eugene et Hibernate. Vous devez utiliser les version suivantes :

  * Eugene >= {{site.topia40xEugeneVersion}}
  * Hibernate >= {{site.topia40xHibernateVersion}}

Des dépendances transitives de ToPIA on changé, il faut remplacer 

{% highlight xml %}
<groupId>org.hibernate</groupId>
{% endhighlight %}

par 

{% highlight xml %}
<groupId>org.hibernate.orm</groupId>
{% endhighlight %}

Et remplacer

{% highlight xml %}
<groupId>javax.persistence</groupId>
<artifactId>javax.persistence-api</artifactId>
<version>2.2</version>
{% endhighlight %}

par

{% highlight xml %}
<groupId>jakarta.persistence</groupId>
<artifactId>jakarta.persistence-api</artifactId>
<version>3.1.0</version>
{% endhighlight %}

## Nom de colonnes désormais interdits

Il semble qu'Hibernate ne fonctionne plus quand des noms de colonnes sont des mot-clés SQL. Par exemple `key` fonctionnait mais ne fonctionne plus, il faut changer le modèle.

## Déclaration des functions SQL

Si vous avez défini des fonctions SQL dans la base de données, vous avez les avez déclarées dans le `Dialect` Hibernate 5 de la façon suivante.

Avant :

{% highlight java %}
public class MyHibernatePostgreSQLDialect extends PostgreSQL95Dialect {

    private static final StandardSQLFunction PREPARE_FOR_FUZZY_SEARCH =
            new StandardSQLFunction("prepare_for_fuzzy_search", StandardBasicTypes.TEXT);

    public EncAhiHibernatePostgreSQLDialect() {
        super();
        registerFunction(PREPARE_FOR_FUZZY_SEARCH.getName(), PREPARE_FOR_FUZZY_SEARCH);
    }

}
{% endhighlight %}

Après :

{% highlight java %}
public class MyHibernatePostgreSQLDialect extends PostgreSQLDialect {

    @Override
    public void initializeFunctionRegistry(FunctionContributions functionContributions) {
        super.initializeFunctionRegistry(functionContributions);
        BasicTypeRegistry basicTypeRegistry = functionContributions.getTypeConfiguration().getBasicTypeRegistry();
        functionContributions.getFunctionRegistry().patternDescriptorBuilder("prepare_for_fuzzy_search", "prepare_for_fuzzy_search(?1)")
                .setInvariantType(basicTypeRegistry.resolve(StandardBasicTypes.STRING))
                .setExactArgumentCount(1)
                .setParameterTypes(FunctionParameterType.STRING)
                .register();
    }
}
{% endhighlight %}

## Création de LOB Hibernate

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Avant :

{% highlight java %}
Session hibernateSession = getHibernateSupport().getHibernateSession();
Blob blob = Hibernate.getLobCreator(hibernateSession).createBlob(inputStream, inputStream.available());
return blob;
{% endhighlight %}

Après :

{% highlight java %}
Session hibernateSession = getHibernateSupport().getHibernateSession();
Blob blob = hibernateSession.getLobHelper().createBlob(inputStream.readAllBytes());
return blob;
{% endhighlight %}


## Changement des directives de configuration

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Les directives de configuration des la connexion JDBC de Hibernate sont à remplacer au profit de celles normées en `jakarta.*`

- `hibernate.connection.driver_class` → `jakarta.persistence.jdbc.driver`
- `hibernate.connection.username` → `jakarta.persistence.jdbc.user`
- `hibernate.connection.password` → `jakarta.persistence.jdbc.password`
- `hibernate.connection.url` → `jakarta.persistence.jdbc.url`

Au delà de ces directives, tout ce qui est en `hibernate.*` peut être à remplacer en `jakarta.persistence.`, à vérifier selon chaque paramètre utilisé.

## Modifications à faire si vous utilisez bean-validation

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Passer de `javax.validation` à `jakarta.validation` dans les directives de configuration.

Ajouter

{% highlight xml %}
<dependency>
    <groupId>org.hibernate.validator</groupId>
    <artifactId>hibernate-validator</artifactId>
    <scope>runtime</scope>
</dependency>
{% endhighlight %}


## Attention aux appels de méthodes visant à forcer Hibernate à charger des champs en lazy

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Des erreurs peuvent apparaître à l’exécution à cause de champs qui ne seraient pas initialisés. Si vous faisiez des appels de méthodes sur des champs d’entités dans l’intention de forcer Hibernate à charger des champs avant de fermer une transaction, il est possible que ces appels ne fonctionnent plus car Hibernate 6 est plus efficace et peut éviter de faire ces chargement inutiles. Une solution est de toujours appeler ces getters mais d’appeler `org.hibernate.Hibernate#initialize(Object)` dessus pour forcer le chargement.

## TopiaMigrationEngine déprécié

![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

Il fonctionne toujours mais est déprécié. Migrez vers Flyway ou Liquibase.