---
layout: documentation
title: "Migrate from ToPIA 2.x, ToPIA 3.x"
category: documentation
---

This documentation section is french-only

## Gestion des dépendances

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

ToPIA nécessite des versions minimum de Eugene et Hibernate. Vous devez utiliser les version suivantes :

ToPIA 3.0

  * Eugene >= {{site.topia30xEugeneVersion}}
  * Hibernate >= {{site.topia30xHibernateVersion}}

ToPIA 3.1.x

  * Eugene >= {{site.topia31+EugeneVersion}}
  * Hibernate >= {{site.topia31xHibernateVersion}}

ToPIA 3.2.x

  * Eugene >= {{site.topia31+EugeneVersion}}
  * Hibernate >= {{site.topia32xHibernateVersion}}


## Configuration du plugin Maven

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Les templates sont contenus dans un modules à part, il faut désormais utiliser
cette dépendance dans le plugin eugene.

{% highlight xml %}
<dependency>
  <groupId>org.nuiton.topia</groupId>
  <artifactId>topia-templates</artifactId>
  <version>{{site.topiaVersion}}</version>
</dependency>
{% endhighlight %}

De plus les templates ont changé de package, le nouveau package est le suivant :

    org.nuiton.topia.templates.*



## Génération des PropertyChangeSupport

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Nous nous sommes aperçus que l'impact des PropertyChangeSupport sur les performances peut ne pas être négligeable. C'est
pourquoi nous avons décidé de désactiver par défaut leur génération. Si vous souhaitez que les PropertyChangeSupport
soient générés, il faut ajouter à votre fichier de properties la tag-value suivante :

{% highlight properties %}
model.tagValue.generatePropertyChangeSupport=true
{% endhighlight %}

Au même titre, les méthodes permettant d'ajouter/supprimer des listener sur les PropertyChangeSupport ne sont plus sur
le contrat TopiaEntity, mais sur le contrat ListenableTopiaEntity. Si vous utilisiez ces méthodes sur votre entité
métier : pas de changement. En revanche si vous utilisiez ces méthodes sur TopiaEntity, vous devez maintenant caster
l'entité ou utiliser directement le bon contrat.

{% highlight java %}
// Topia 2.x
TopiaEntity anEntity = ...;
anEntity.addPropertyChangeListener(listener);

// Topia 3 (cast)
TopiaEntity anEntity = ...;
if (anEntity instanceof ListenableTopiaEntity) {
    ((ListenableTopiaEntity)anEntity).addPostWriteListener(listener);
}

// Topia 3 (direct)
ListenableTopiaEntity anEntity = ...;
anEntity.addPostWriteListener(listener);
{% endhighlight %}

D'une manière générale, si toutes vos entités sont des ListenableTopiaEntity, nous vous encourageons à utiliser ce
contrat à la place de TopiaEntity.

Ces méthodes ont également été renommées, voici les correspondances :

    `addVetoableListener`          -> `addPreReadListener`
    `removeVetoableListener`       -> `removePreReadListener`
    `addPropertyListener`          -> `addPostReadListener`
    `removePropertyListener`       -> `removePostReadListener`
    `addVetoableChangeListener`    -> `addPreWriteListener`
    `removeVetoableChangeListener` -> `removePreWriteListener`
    `addPropertyChangeListener`    -> `addPostWriteListener`
    `removePropertyChangeListener` -> `removePostWriteListener`

À noter que les méthodes suivantes existent toujours, mais sont dorénavant isolées sur un autre contrat (ListenableBean) :

    `addPropertyChangeListener`
    `removePropertyChangeListener`


## Refonte des indexed/ordered

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Vous devez migrer votre modèle zargo depuis la beta-3 suite à une confusion entre ordered et indexed (stéréotypes
à placer sur une association-end d'un lien entre deux entités).

  * Le stéréotype "indexed" est déprécié, vous devez le remplacer par l'attribut "ordered"
  * L'attribut (ou le stéréotype) "ordered" maintient l'ordre d'insertion en base, il faut donc vous assurer qu'une
    colonne <champ>_idx est bien présente dans la base de donnée et au besoin écrire la migration nécessaire 
    pour ajouter la colonne et fixer les indexes manquants (en commençant à 0).
  * Les types des collections générées ont changé afin d'utiliser un contrat plus proche du modèle (selon la présence
    des stéréotypes "unique" et/ou "ordered").

NB : La documentation contient des [examples de migration pour la refonte des indexed/ordered].


## Code déprécié

La migration vers ToPIA 3.0 voit un certain nombre de classes et attributs dépréciés.

Gérer les impacts n'est pas *nécessaire* pour passer à ToPIA 3.0, mais c'est fortement recommandé. En effet, ce code
deviendra incompatible avec ToPIA 3.1, gérer les impacts au plus tôt permettra une migration en douceur.

Pas vrai, à partir de la 3.0-alpha-6, on casse la rétro-compatibilité car sinon
c'est trop compliqué à gérer.


## Propriétés TOPIA_*

![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

Les constantes suivantes sont à remplacer par leurs homologues avec le prefix *PROPERTY_* :

  * `TopiaEntity#TOPIA_ID` devient `TopiaEntity#PROPERTY_TOPIA_ID`
  * `TopiaEntity#TOPIA_CREATE_DATE` devient `TopiaEntity#PROPERTY_TOPIA_CREATE_DATE`
  * `TopiaEntity#TOPIA_VERSION` devient `TopiaEntity#PROPERTY_TOPIA_VERSION`
  * `TopiaEntityContextable#TOPIA_CONTEXT` devient `TopiaEntityContextable#PROPERTY_TOPIA_CONTEXT`
  * `TopiaEntity#COMPOSITE` devient `TopiaEntityContextable#PROPERTY_COMPOSITE`
  * `TopiaEntity#AGGREGATE` devient `TopiaEntityContextable#PROPERTY_AGGREGATE`


## TopiaId

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

La classe sera supprimée. Pour manipuler les topiaId, il faut utiliser le **TopiaIdFactory**.

La forme des topiaIds générés à changé. Si vous souhaitez conserver l'ancienne forme parce que vous
souhaitez conserver l'uniformité avec une base de données existante ou si vous utilisez
topia-service-security, vous devez explicitement le spécifier à Topia 3.0 via la configuration :

{% highlight properties %}
topia.persistence.topiaIdFactoryClassName=org.nuiton.topia.persistence.internal.LegacyTopiaIdFactory
{% endhighlight %}


## TopiaContextImplementor est supprimé

Cette classe joue un rôle central dans ToPIA 2. Elle n'existe plus dans ToPIA 3.

TODO Expliquer par quoi remplacer.


## TopiaEntity modifié

Les méthodes (`getComposite()`, `getAggregate()`) ont été déplacées vers le contrat [TopiaEntityContextable].


## TopiaService#preInit(...) et TopiaService#postInit(...)

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Les services implémentant **TopiaService** doivent changer leur code.
Les méthodes `preInit(TopiaContextImplementor)` et `postInit(TopiaContextImplementor)` doivent être remplacées par
`preInit(TopiaContext)` et `postInit(TopiaContext)`


## Service de migration

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Toute référence à **TopiaContextImplementor** a été supprimé et remplacée par
**TopiaContext**, cela change donc les signatures et rend le code incompatible,
il faut donc bien effectuer le remplacement dans les callbacks que vous avez
écrits.


## Gestion des exceptions

![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

**TopiaRuntimeException** est dépréciée. Il faut désormais déclarer **TopiaException** dans la clause catch.
**TopiaException** devient Runtime. Il n'est plus nécessaire de l'intercepter.


## Généricité

![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

Les methodes suivantes sont désormais génériques :

 * `TopiaContext#find(...)`
 * `TopiaContext#findAll(...)`
 * `TopiaContext#findUnique(...)`

Si le code précédent réalisait un cast, il faut le supprimer car cela peut causer maintenant une erreur de compilation.


## TopiaContextListener

![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

L'API **TopiaContextListener** contenant des opérations de manipulation du schéma de base de données a été déprécié.
L'interface est remplacée par **TopiaSchemaListener**. Les méthodes présentes sur le **TopiaContext** qui servaient à
ajouter/supprimer un **TopiaContextListener** sont elles aussi dépréciées au profit de méthodes manipulant des
**TopiaSchemaListener**.


## Transformer pour la génération des Dao


![RECOMMENDED]({{site.baseurl}}/assets/recommended.png)

Si vous utilisiez explicitement le Transformer de DAO dans la configuration de votre plugin Eugene, il faut maintenant
changer le nouveau Transformer : org.nuiton.topia.templates.EntityDaoTransformer

Exemple de configuration :

{% highlight xml %}
<plugin>
  <groupId>org.nuiton.eugene</groupId>
  <artifactId>eugene-maven-plugin</artifactId>
  <executions>
    <execution>
      <id>generate-entities</id>
      <phase>generate-sources</phase>
      <configuration>
        <inputs>classpath:model:/:myProject.objectmodel</inputs>
        <defaultPackage>org.project.entities</defaultPackage>
        <templates>
          org.nuiton.topia.templates.EntityDaoTransformer
        </templates>
      </configuration>
      <goals>
        <goal>generate</goal>
      </goals>
    </execution>
  </executions>
</plugin>
{% endhighlight %}



![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Si vous aviez surchargé certains des DAO générés, vous devrez probablement les renommer et changer leur signature.
Surveillez les logs de génération des DAO à la recherche des messages tels que :

{% highlight bash %}
  WARN [main] (EntityDaoTransformer.java:384) warnOnLegacyClassDetected - Legacy DAO detected !
   - You should consider renaming 'org.project.entities.ZoneDAOImpl' to 'org.project.entities.AbstractZoneTopiaDao'
   - Expected class declaration is : public class AbstractZoneTopiaDao<E extends Zone> extends GeneratedZoneTopiaDao<E> {
{% endhighlight %}

Pour l'entité Zone, vous devez supprimer votre ZoneDAO et votre ZoneDAOImpl et déplacer les méthodes
* dans ZoneTopiaDao si la méthode ne peut être utilisée seulement pour l'entité Zone (et pas les sous-classes de Zone)
* dans AbstractZoneTopiaDao si la méthode peut être utilisée pour Zone ET les entités qui héritent de Zone


## Méthodes findAll, find et findUnique sur TopiaContext


![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Si vous utilisiez ces méthodes, il est désormais recommandé d'utiliser les méthodes sur le DAO associé à votre éntité
ou par l'intermédiare du **TopiaJpaSupport**.


### Depuis un DAO surchargé

Pour faciliter la migration, ces méthodes ont été portées sur l'interface deprecated **TopiaDAO**. Donc si dans votre
DAO vous faisiez :

{% highlight java %}
List<Zone> zones = context.find("FROM Zone WHERE name=:name" , startIndex, endIndex, "name", "Principale");
{% endhighlight %}

Il vous suffit de retirer le "context." :

{% highlight java %}
List<Zone> zones = find("FROM Zone WHERE name=:name" , startIndex, endIndex, "name", "Principale");
{% endhighlight %}


### Dans tous les cas

Dans tous les cas, ces méthodes étant dépréciées, vous devriez consulter leur Javadoc pour savoir par quels appels de
méthodes les remplacer. S'il n'y pas de documentation, il faut remplacer le code appellant par le corps de la méthode dépréciée
(fonctionnalité « inline » dans l'IDE).


## Amélioration du code sql

Voir [TopiaSqlQuery], [TopiaSqlWork] et [TopiaSqlSupport].


## Remaniement des packages

![MANDATORY]({{site.baseurl}}/assets/mandatory.png)

Un grand nombre de classes ont été déplacées. Voici les principaux renommages :

    org.nuiton.topia.TopiaException -> org.nuiton.topia.persistence.TopiaException
    org.nuiton.topia.TopiaDaoSupplier -> org.nuiton.topia.persistence.TopiaDaoSupplier
    org.nuiton.topia.TopiaJpaSupport -> org.nuiton.topia.persistence.support.TopiaJpaSupport
    org.nuiton.topia.TopiaPersistenceContext -> org.nuiton.topia.persistence.TopiaPersistenceContext
    org.nuiton.topia.TopiaNotFoundException -> org.nuiton.topia.persistence.TopiaNotFoundException
    org.nuiton.topia.TopiaApplicationContextCache -> org.nuiton.topia.persistence.TopiaApplicationContextCache
    org.nuiton.topia.TopiaTransaction -> org.nuiton.topia.persistence.TopiaTransaction
    org.nuiton.topia.persistence.TopiaHibernateSessionRegistry -> org.nuiton.topia.persistence.internal.TopiaHibernateSessionRegistry
    org.nuiton.topia.TopiaListenableSupport -> org.nuiton.topia.persistence.support.TopiaListenableSupport
    org.nuiton.topia.persistence.HibernateProvider -> org.nuiton.topia.persistence.internal.HibernateProvider
    org.nuiton.topia.TopiaContextFactory -> org.nuiton.topia.persistence.TopiaConfigurationConstants


Le refactoring peut s'effectuer facilement en appliquant les commandes suivantes :

{% highlight bash %}
$ grep -m 1 -nr 'org.nuiton.topia.TopiaException' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaException/org.nuiton.topia.persistence.TopiaException/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaDaoSupplier' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaDaoSupplier/org.nuiton.topia.persistence.TopiaDaoSupplier/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaJpaSupport' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaJpaSupport/org.nuiton.topia.persistence.support.TopiaJpaSupport/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaPersistenceContext' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaPersistenceContext/org.nuiton.topia.persistence.TopiaPersistenceContext/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaNotFoundException' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaNotFoundException/org.nuiton.topia.persistence.TopiaNotFoundException/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaApplicationContextCache' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaApplicationContextCache/org.nuiton.topia.persistence.TopiaApplicationContextCache/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaTransaction' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaTransaction/org.nuiton.topia.persistence.TopiaTransaction/g'
$ grep -m 1 -nr 'org.nuiton.topia.persistence.TopiaHibernateSessionRegistry' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.persistence.TopiaHibernateSessionRegistry/org.nuiton.topia.persistence.internal.TopiaHibernateSessionRegistry/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaListenableSupport' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaListenableSupport/org.nuiton.topia.persistence.support.TopiaListenableSupport/g'
$ grep -m 1 -nr 'org.nuiton.topia.persistence.HibernateProvider' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.persistence.HibernateProvider/org.nuiton.topia.persistence.internal.HibernateProvider/g'
$ grep -m 1 -nr 'org.nuiton.topia.TopiaContextFactory' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/org.nuiton.topia.TopiaContextFactory/org.nuiton.topia.persistence.TopiaConfigurationConstants/g'
$ grep -m 1 -nr 'TopiaContextFactory.CONFIG_' . | awk -F ':' '{print $1}' | xargs sed -i -e 's/TopiaContextFactory.CONFIG_/TopiaConfigurationConstants.CONFIG_/g'
{% endhighlight %}

## FAQ

Voici quelques appels faisables en ToPIA 2.x et leur équivalent dans ToPIA 3

## Démarrage d'une nouvelle transaction

{% highlight java %}
// ToPIA 2.x : rootTx étant le TopiaContext root
TopiaContext transaction = rootTx.beginTransaction();

// ToPIA 3 : rootTx étant le MyModelTopiaApplicationContext
MyModelTopiaPersistenceContext transaction = rootTx.newPersistenceContext();
// Note : MyModelTopiaPersistenceContext implémente le contrat TopiaTransaction
{% endhighlight %}


## Accès à un Dao depuis un autre Dao

{% highlight java %}
// ToPIA 2.x
SpeciesDAO speciesDAO = MyModelDAOHelper.getSpeciesDAO(getContext());

// ToPIA 3
SpeciesTopiaDao speciesDAO = topiaDaoSupplier.getDao(Species.class, SpeciesTopiaDao.class);
{% endhighlight %}


## Accès à la session Hibernate

{% highlight java %}
// ToPIA 2.x : tx étant le TopiaContext
((MyModelTopiaPersistenceContextImplementor) tx).getHibernate().flush();

// ToPIA 3 : tx étant le MyModelTopiaPersistenceContext
tx.getHibernateSupport().getHibernateSession().flush();
{% endhighlight %}


## Liste des classes d'implémentation (ou des contrats)

{% highlight java %}
// ToPIA 2.x
MyModelDAOHelper.getContractClasses();
MyModelDAOHelper.getImplementationClasses();

// ToPIA 3
MyModelEntityEnum.getContractClasses();
MyModelEntityEnum.getImplementationClasses();
{% endhighlight %}


## TopiaPersistenceHelper renommé

Le contrat **TopiaPersistenceHelper** est renommé en [TopiaEntityEnumProvider]. À noter que le **MyModelTopiaApplicationContext**
implémente ce contrat.


[Créer un projet ToPIA en partant de rien]: {{site.baseurl}}/documentation/from_scratch.html
[migrate_to_topia_3]:                       {{site.baseurl}}/documentation/migrate_to_topia_3.html
[ordered_to_indexed_migration]:             {{site.baseurl}}/documentation/ordered_to_indexed_migration.html
[TopiaSqlQuery]:                            {{site.apidocs}}?org/nuiton/topia/persistence/support/TopiaSqlQuery.html
[TopiaSqlWork]:                             {{site.apidocs}}?org/nuiton/topia/persistence/support/TopiaSqlWork.html
[TopiaSqlSupport]:                          {{site.apidocs}}?org/nuiton/topia/persistence/support/TopiaSqlSupport.html
[TopiaEntityEnumProvider]:                  {{site.apidocs}}?org/nuiton/topia/persistence/TopiaEntityEnumProvider.html
[TopiaEntityContextable]:                   {{site.apidocs}}?org/nuiton/topia/persistence/TopiaEntityContextable.html
[le guide dédié à la migration ordered]:    {{site.baseurl}}/documentation/ordered_vs_indexed_migration.html
[examples de migration pour la refonte des indexed/ordered]:    {{site.baseurl}}/documentation/ordered_vs_indexed_migration.html

## Changement de l'API de configuration de ToPIA

À partir de ToPIA 3.0-beta-16, des configurations par défaut ont changés. Pour être iso-fonctionnel vous devez préciser dans votre configuration les anciennes valeurs par défaut :

{% highlight properties %}
hibernate.ejb.naming_strategy=org.hibernate.cfg.DefaultNamingStrategy
{% endhighlight %}

Il n'est plus possible de fixer la propriété hibernate.hbm2ddl.auto : vous devez utiliser l'API de migration.
