---
layout: documentation
title: "Just-In-Time schema migrations with Flyway"
category: documentation
---

[Flyway] manages your database migrations. It uses a database table named `schema_version` which contains information
about the past migrations.

ToPIA provides a Flyway integration as a service, here is how to setup it.


## Setup

First thing to do when setting up Flyway, is to add the dependency to your project:

{% highlight xml %}
<dependency>
  <groupId>org.nuiton.topia</groupId>
  <artifactId>topia-service-flyway</artifactId>
  <version>{{site.topiaVersion}}</version>
</dependency>
{% endhighlight %}

Then you have to declare the migration engine by adding the following line to your configuration:

{% highlight properties %}
topia.service.migration=org.nuiton.topia.flyway.TopiaFlywayServiceImpl
{% endhighlight %}


## Write your migration scripts

You need to declare your migration scripts in the `src/main/resources/db/migration/` folder of your module.

Your migration scripts file names has to respect the following formalism `V1_1_0_3__add_required_columns_to_store_user_profile.sql`. The name has to be
such as:

  * `V`: mandatory letter at the beginning of the file name;
  * `1_1_0_3`: script's version (aka 1.1.0.3);
  * `__`: double underscore (_) as a separator;
  * `add_required_columns_to_store_user_profile`: description of the migration (using underscores as spaces);
  * `.sql`: the file extension.

The different scripts are then applied according to the version numbers. More information on [Flyway's website].


## Configuration

### Installing Flyway on an existing database

If you start ToPIA in the following context:

  - Flyway was never configured before
  - You just declared the Flyway service and this is the first time ToPIA
    will boot with Flyway enabled
  - ToPIA will start and the schema already exists

ToPIA will fail to boot for an obvious reason:

  - the schema exists
  - the flyway table does not

So, the schema already exists but Flyway, which was not enabled when the
schema was created, can not guess the schema version. Thus, Flyway can't
know which migration already passed, and which remain to run.

To solve that, you must tells Flyway to initialize and provide flyway
with the actual version of the existing shema. This version is called
`baseline` version. If you set the baseline version to `3`, Flyway will
ignore all migration to version `3`, including `3` and apply only the
remaining ones.

Example:

{% highlight properties %}
topia.service.migration.flyway.initVersion=1.0.1.2
{% endhighlight %}

You can learn more by reading [Flyway's documentation about installing
Flyway on an existing database].

### Ignore model version when choosing migrations to run

By default, Flyway will run every migrations found up to the model version.

If the model version is `3`, and there is a migration name `V4`, the
migration will not be run.

It is possible to tell Flyway to apply all available migrations and ignore
the model version. To do that, just add the next line:

{% highlight properties %}
topia.service.migration.useModelVersion=false
{% endhighlight %}


[Flyway's website]: https://flywaydb.org
[Flyway]:           https://flywaydb.org
[Flyway's documentation about installing Flyway on an existing database]: https://flywaydb.org/documentation/existing