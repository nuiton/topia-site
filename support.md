---
layout: page
title: "Support"
---

## Mailing-lists

We would be happy to hear from you and lend a hand by email, you can subscribe to [topia-users, our public mailing list].

## Paid support

We can provide support and training for ToPIA wheter it is before, while or after adoption.

Feel free to [contact us], prices are available on [Code Lutin's website].

[contact us]: mailto:sales@codelutin.com
[Code Lutin's website]:                                https://www.codelutin.com/
[topia-users, our public mailing list]:              https://ml.nuiton.org/cgi-bin/mailman/listinfo/topia-users
